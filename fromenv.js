//  be sure to 
//  npm install dotenv
// https://medium.com/the-node-js-collection/making-your-node-js-work-everywhere-with-environment-variables-2da8cdf6e786
console.log(`Your integer is null ${process.env.ANINT}`); // undefined
const dotenv = require('dotenv').config();
console.log(`Now your integer is ${process.env.ANINT}`);
console.log(`Your password is ${process.env.PASSWORD}`);
