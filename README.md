datadotenv
==========

In data science workflows there is always a risk of publishing your passwords, API keys, and path information.  As flows grow across scripts and languages, there's a need to very simply share the same sensitive information across scripts and languages.  With `datadotenv` you can access these variables across postgres, Python, R, Docker, shell, and JavaScript (and more?) with base functionality in a way that protects you from $20,000 dollar mistakes like committing your Amazon MTurk API keys, for example.  

SETUP
-----

Think of this as very minimal, non-bossy boilerplate to pull into each project you start.

To start, Don't clone the repo, just copy `.env.sample` into your project as `.env`, add your variables, and crib relevant import code from the `.R`, `.py`, or whichever sample import script.  

Use `.env` to store path information, URLs, passwords, API keys, anything else that you want to be common to all languages in your flow, anything else that you shouldn't put online or commit to git

You'll now start each programming session pulling the variables from `.env` into your environment (code below).



Use
---

There is no avoiding the two basic steps: getting the data from your `.env` to your session's environment, and getting it from the environment to whichever script in whichever language.

To do the first part, follow setup above and then type
```
set -a
. ./.env
set +a
```
or 
```
set -a && . ./.env && set +a
```
or
```
export $(grep -v '^#' .env | xargs)
```
or [something](https://stackoverflow.com/questions/7507810/how-to-source-a-script-in-a-makefile) [else](https://stackoverflow.com/questions/19331497/set-environment-variables-from-file-of-key-value-pairs)

You have to do this every time you start work, or get it called by some other startup script (or use make). Then follow the example in each language's sample script to import from env and print to console.



Use with `make`
---

For the more efficient but more opinionated approach, you can use make.  To see this code in action via make, type
```
cat .env
make testshell
make testr
make testpy
make testsql
make testjs
```

These two lines at the top of the make file provide access to `.env` from *all* targets.
```
include .env
.EXPORT_ALL_VARIABLES:
```
This approach lets you continue, business as usual, persisting in the illusion that each `make` target is a little shell script.   

## Also
Commit a customized `.env.sample` to your project with the local variables that another user should set to get your project up on their machine.

You might also add your `.env` to `.gitignore` to further protect you from accidentally committing.



Design choices
--------------

*  Why environment variables?
   *  They're used for just this kind of thing all the time.
   *  They're very easily supported by every language.
   *  They're considered secure by the best practices of web services (see "12 factor" or something).
   *  Already supported in the unix ecosystem. Get support not just in Python, R, and JS, but shell and Docker for free.
      *  Most likely of all approaches to be easy to grow out to Julia or whatever.
*  Why `.env`?
   *  Simple generic format in one file, without gotchas, with comments, 
   *  Having a `local_settings.js/py/R` is three files instead of one.
   *  Better than JSON. JSON is:
      *  great, 
	  *  but also overpowered for this problem, and 
	  *  finicky (final commas, double quotes only), 
	  *  doesn't support comments,
	  *  not as brainless as you'd think to import with base functionality into all languages you'd want.
*  Why `make`?
   *  You really don't have to use the `make` part of this, just ignore it.  In fact,
   *  You likely shouldn't if you're only staying within one language or one script, but
   *  If you want to know why you might, Google _"gnu make in data science"_.  
   *  Using make, there's less typing, better organization as flows grow.
   *  In the approach here, make targets are just as hard as usually to tell from little shell scripts.  A data scientist shouldn't have to know that those things are different.  I was much happier before I knew. With `datadotenv` you can be happy.


Notes 
-----
Author is [Seth Frey](https://enfascination.com/research). Let me know if you extend to other languages, like Julia or whatever.
