include .env
.EXPORT_ALL_VARIABLES:

testpy:
	python fromenv.py

testr: 
	Rscript fromenv.R

testjs: 
	npm install dotenv
	npm install process
	node fromenv.js

testsql:
	#  this assumes sqlite. pick your poison.  every sql import from the environment in a different way.
	#. ./.env
	sqlite3 ex1 '.shell echo ${PASSWORD}'


test: testpy testr testjs testsql
